FROM alpine:3.9

#VOLUME /data/storage
MAINTAINER Eduardo Dinato <eduardo.dinato@prf.gov.br>

# Apaga arquivos mais antigos que esse limite de dias:
ENV DAY_LIMIT=30
# Diretório que será usado pra guardar os dados do servidor sftp
ENV STORAGE_PATH=/sftp-server/data
# Arquivo contendo usuários na seguinte sintaxe: [user]:[senha]:[uid] em cada linha.
ENV USERS_FILE=/opt/app/users_list.txt

RUN apk update && apk --update --no-cache add tzdata bash lftp openssh-client openssh-server openssh-keygen openssh-sftp-server rsyslog

# Instala a timezone de Porto Velho e limpa o cache do APK
RUN cp /usr/share/zoneinfo/America/Porto_Velho /etc/localtime 
RUN echo "America/Porto_Velho" > /etc/timezone  
RUN apk del tzdata

# Limpa o cache
RUN rm -rf /var/cache/apk/* 

# Cria os diretórios necessários
RUN mkdir -p /sftp-server/data/
RUN mkdir -p /var/log/cron/
RUN mkdir -p /opt/app
RUN mkdir /var/run/sshd
# Gera a chave do servidor ssh
RUN ssh-keygen -A

# Copia os Assets do aplicativo
COPY assets/app/ /opt/app/

RUN /opt/app/setup_ssh.sh

RUN chmod 755 /opt/app/cron_jobs.txt

# Ajusta permissões do crontab
RUN chown root:root /etc/crontabs/root

RUN /usr/bin/crontab /opt/app/cron_jobs.txt


# Abre portas pro protocolo FTP
EXPOSE 22

ENTRYPOINT ["/opt/app/docker-entrypoint.sh"]

# roda o serviço do CRON e o loop pra manter o serviço vivo
CMD ["/opt/app/docker-cmd.sh"]
