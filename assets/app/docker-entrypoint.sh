#!/bin/sh

###########################################
#
# Entrypoint
#
###########################################

#Diretório absoluto do script
SCRIPT=$(readlink -f "$0")
DIR=$(dirname "$SCRIPT")

set -e
/opt/app/create_users.sh

echo "Executando comando $@..."
exec "$@"
