
#!/bin/bash

#######
# Usuário deve ser listado no formato nome:senha, exemplo: 
# edu:1234
# nomeTeste:senhaDoUsuario!
# usuario3:senha3

# Cria o grupo sftp
addgroup --gid 2000 sftp

while read p; do
    user=$(echo "$p" | cut -d':' -f1)
    senha=$(echo "$p" | cut -d':' -f2)        
    uid=$(echo "$p" | cut -d':' -f3)
    echo "Criando usuário $user..."        
    adduser -u $uid -D $user -G sftp
    echo "$user:$senha" | chpasswd
    mkdir -p /sftp-server/data/$user
    chown $user:sftp /sftp-server/data/$user
    chmod 700 /sftp-server/data/$user    
done <$USERS_FILE
