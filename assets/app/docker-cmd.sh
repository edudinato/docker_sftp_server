#!/bin/sh

# apaga o último log do cron
rm -rf /var/log/cron/cron.log

echo "Iniciando cron..."
/usr/sbin/crond -b

echo "Iniciando servidor SFTP..."

set -e

### Retirado de https://github.com/xordiv/docker-alpine-cron/blob/master/scripts/docker-cmd.sh
# crond running in background and log file reading every second by tail to STDOUT    
# echo "Executando o serviço 'Crontab Daemon' no foreground..."
# crond -S /var/spool/cron/crontabs -f -L /var/log/cron/cron.log "$@"

#x=0
#while :
#do            
    #sleep 1
#done
#executa o servidr ssh
/opt/app/create_users.sh

/usr/sbin/sshd -D
