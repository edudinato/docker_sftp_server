# Apaga arquivos mais antigos que 30 dias
* * * * * find $STORAGE_PATH -mtime +$DAY_LIMIT -type f -delete
# An empty line is required at the end of this file for a valid cron file.
