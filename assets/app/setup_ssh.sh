#!/bin/bash


#############################3
#
# Faz as configurações necessárias no SSH Server
#
#

echo "  SyslogFacility AUTH" >>  /etc/ssh/sshd_config
echo "  LogLevel VERBOSE" >>  /etc/ssh/sshd_config

echo "Match group sftp" >> /etc/ssh/sshd_config
echo "  ChrootDirectory /sftp-server/data/" >> /etc/ssh/sshd_config
echo "  ForceCommand internal-sftp -d /%u" >>  /etc/ssh/sshd_config
echo "  X11Forwarding no" >>  /etc/ssh/sshd_config
echo "  AllowTcpForwarding no" >>  /etc/ssh/sshd_config
